{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Maximum Entropy POS Tagger\n",
    "\n",
    "> __Zusatzaufgabe Linguistische Informatik - Praktische Übung__  \n",
    "> *Jonathan Schlue* \\#3712892  \n",
    "> Dieser Tagger basiert auf dem [Paper von Adwait Ratnaparkhi](http://www.aclweb.org/anthology/W96-0213)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Vorbereitung\n",
    "\n",
    "Im Projektverzeichnis können in der Konsole mithilfe von [conda](https://conda.io/miniconda.html) alle Abhängigkeiten wie folgt installiert werden:\n",
    "\n",
    "```sh\n",
    "conda env create --file environment.yml\n",
    "source activate li-lab-env\n",
    "python\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# imports\n",
    "\n",
    "import nltk\n",
    "from nltk.corpus import treebank\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import maxentropy\n",
    "import random"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Daten\n",
    "\n",
    "Als Datengrundlage verwende ich den Penn Treebank Korpus mit dem Universalen Tagset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# install corpus and tagset\n",
    "\n",
    "nltk.download('treebank')\n",
    "nltk.download('universal_tagset')\n",
    "sentences = list(treebank.tagged_sents(tagset='universal'))\n",
    "len_sentences = len(sentences)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Split Trainings- und Testdaten"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# randomly draw training and test data partitions\n",
    "random.shuffle(sentences)\n",
    "\n",
    "# split training and test data\n",
    "TRAINING_TEST_SPLIT = int(.9 * len_sentences)\n",
    "sentences_training, sentences_test = sentences[:TRAINING_TEST_SPLIT], sentences[TRAINING_TEST_SPLIT:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Datenkonvertierung und Termfrequenzen\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "words = [] # tokens\n",
    "TAGS = set() # unique POS Tags found in corpus\n",
    "tag_dict = {} # word -> tags dictionary\n",
    "for sentence in sentences_training:\n",
    "    for word, tag in sentence:\n",
    "        \n",
    "        # collect words and tags\n",
    "        words.append(word)\n",
    "        TAGS.add(tag)\n",
    "        \n",
    "        # fill the dictionary\n",
    "        tag_dict_entry = tag_dict.get(tag, set())\n",
    "        tag_dict_entry.add(tag)\n",
    "        tag_dict[word] = tag_dict_entry\n",
    "        \n",
    "# calculate token frequencies\n",
    "freq = pd.Series(words).value_counts()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## TRAINING\n",
    "\n",
    "### Berechnung des Sample-Spaces\n",
    "\n",
    "Zunächst werden aus den Annotierten Sätzen der Trainingsdaten Samples erhoben.  \n",
    "Jedes Sample besteht dabei aus einem POS-Tag *t_i* und einem Kontext *h_i*, der sich konkret in  \n",
    "umliegenden Wörtern und POS-Tags veräußert."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def generate_samples(sentence: list) -> list:\n",
    "    \"\"\"Generate data samples from sentence.\n",
    "    \n",
    "    Each sample contains the context of words (window size of 5)\n",
    "    and the two preceeding tags.\n",
    "    \"\"\"\n",
    "    \n",
    "    \n",
    "    def get(key: int, index: int, sentence: list) -> str:\n",
    "        \"\"\"Extract word or tag from the token at position `index` in the given sentence.\n",
    "        \n",
    "        Use key 0 to get the word, key 1 to get the token.\n",
    "        Defaults to None, if the index is out of bounds.\n",
    "        \"\"\"\n",
    "        return sentence[index][key] if 0 <= index and index < len(sentence) else None\n",
    "    \n",
    "    \n",
    "    samples = []\n",
    "    for i in range(len(sentence)):     \n",
    "        context = []\n",
    "        target = get(1, i, sentence)\n",
    "        word_window = range(i-2, i+3)\n",
    "        tag_window = range(i-2, i)\n",
    "        context += [get(0, j, sentence) for j in word_window]\n",
    "        context += [get(1, j, sentence) for j in tag_window]\n",
    "        samples.append(context + [target])\n",
    "    return samples\n",
    "\n",
    "# generate samples from corpus\n",
    "\n",
    "sample_data = []    \n",
    "for sentence in sentences_training:\n",
    "    [sample_data.append(sample) for sample in generate_samples(sentence)]\n",
    "sample_data = pd.DataFrame(sample_data)\n",
    "SAMPLE_DATA_COLUMNS = [\"w_i-2\", \"w_i-1\", \"w_i\", \"w_i+1\", \"w_i+2\", \"t_i-2\", \"t_i-1\", \"t_i\"]\n",
    "sample_data.columns = SAMPLE_DATA_COLUMNS\n",
    "\n",
    "sample_data[:10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Unterscheidung von seltenen und nicht seltenenen Wörtern\n",
    "\n",
    "Ich folge hier der Annahme im Paper, dass selten Wortformen, die in den Trainingsdaten selten vorkommen, sich ähnlich verhalten wir Wortformen, die in den Testdaten erstmals auftreten.\n",
    "\n",
    "Diese Unterscheidung schlägt sich maßgeblich in der folgenden Auswahl der Feature-Templates wieder."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# split into rare and non-rare tokens\n",
    "\n",
    "RARE_TOKEN_SPLIT = 5\n",
    "rare = list(freq[freq < RARE_TOKEN_SPLIT].index)\n",
    "\n",
    "sample_data['w_i_rare'] = sample_data['w_i'].isin(rare)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Feature-Templates\n",
    "\n",
    "Die Feature-Templates sind direkte Implementierungen der Templates in Tabelle 1 im Paper. Die Modellierung mit Identifizierungstupeln dient im nächsten Schritt der Auschlöschung doppelter, identischer Features."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define feature templates\n",
    "\n",
    "# not rare\n",
    "ft0  = lambda X, T:    (( 0, X, T),    lambda x: x['w_i'] == X                       and x['t_i'] == T)\n",
    "\n",
    "# rare\n",
    "ft1  = lambda X, T:    (( 1, X, T),    lambda x: x['w_i'].startswith(X)              and x['t_i'] == T)\n",
    "ft2  = lambda X, T:    (( 2, X, T),    lambda x: x['w_i'].endswith(X)                and x['t_i'] == T)\n",
    "ft3  = lambda T:       (( 3, T),       lambda x: any(c.isdigit() for c in x['w_i'])  and x['t_i'] == T)\n",
    "ft4  = lambda T:       (( 4, T),       lambda x: any(c.isupper() for c in x['w_i'])  and x['t_i'] == T)\n",
    "ft5  = lambda T:       (( 5, T),       lambda x: '-' in x['w_i']                     and x['t_i'] == T)\n",
    "\n",
    "# all\n",
    "ft6  = lambda X, T:    (( 6, T),       lambda x: x['t_i-1'] == X                     and x['t_i'] == T)\n",
    "ft7  = lambda X, Y, T: (( 7, X, Y, T), lambda x: x['t_i-2'] == X and x['t_i-1'] == Y and x['t_i'] == T)\n",
    "ft8  = lambda X, T:    (( 8, X, T),    lambda x: x['w_i-1'] == X                     and x['t_i'] == T)\n",
    "ft9  = lambda X, T:    (( 9, X, T),    lambda x: x['w_i-2'] == X                     and x['t_i'] == T)\n",
    "ft10 = lambda X, T:    ((10, X, T),    lambda x: x['w_i+1'] == X                     and x['t_i'] == T)\n",
    "ft11 = lambda X, T:    ((11, X, T),    lambda x: x['w_i+2'] == X                     and x['t_i'] == T)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Feature-Instanzierung\n",
    "\n",
    "Ausgehend von den oben implementierten Feature-Templates können nun anhand der Trainingsdaten konkrete Features abgeleitet werden. Je nach Häufigkeit des zentralen Wortes werden hier pro Kontext unterschiedliche Features instanziert.\n",
    "\n",
    "Das Dictionary dient hier der o.g. Dupkikatvermeidung von Features."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "feature_dict = {}\n",
    "\n",
    "\n",
    "def prefixes(w: str) -> list:\n",
    "    \"\"\"Returns 0 to 4 non-empty prefixes of w\"\"\"\n",
    "    return [w[:l] for l in range(1, 1 + min(len(w), 4))]\n",
    "\n",
    "\n",
    "def suffixes(w: str) -> list:\n",
    "    \"\"\"Returns 0 to 4 non-empty suffixes of w\"\"\"\n",
    "    return [w[-l:] for l in range(1, 1 + min(len(w), 4))]\n",
    "    \n",
    "    \n",
    "def instantiate_features(s: pd.Series) -> None:\n",
    "    \"\"\"Instantiate features necessary to encode this sample.\"\"\"\n",
    "    features = []\n",
    "    if not s['w_i_rare']:\n",
    "        # non rare\n",
    "        # TODO do not filter ft0 instances as rare\n",
    "        features.append(ft0(s['w_i'], s['t_i']))\n",
    "    else:\n",
    "        \n",
    "        # rare\n",
    "        features += [ft1(prefix, s['t_i']) for prefix in prefixes(s['w_i'])]\n",
    "        features += [ft2(suffix, s['t_i']) for suffix in suffixes(s['w_i'])]\n",
    "        features.append(ft3(s['t_i']))\n",
    "        features.append(ft4(s['t_i']))\n",
    "        features.append(ft5(s['t_i']))\n",
    "        \n",
    "    # all\n",
    "    features.append( ft6(s['t_i-1'], s['t_i']))\n",
    "    features.append( ft7(s['t_i-2'], s['t_i-1'], s['t_i']))\n",
    "    features.append( ft8(s['w_i-1'], s['t_i']))\n",
    "    features.append( ft9(s['w_i-2'], s['t_i']))\n",
    "    features.append(ft10(s['w_i+1'], s['t_i']))\n",
    "    features.append(ft11(s['w_i+2'], s['t_i']))\n",
    "    \n",
    "    # globally collect features and remove duplicates\n",
    "    for feature in features:\n",
    "        feature_dict[feature[0]] = feature[1]\n",
    "        \n",
    "\n",
    "sample_data.apply(instantiate_features, axis=1)\n",
    "feature_list = list(feature_dict.values())\n",
    "feature_list[:10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Berechnung des Feature-Spaces"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def feature_vector(x: pd.Series) -> list:\n",
    "    \"\"\"Calculate the feature vector for a given sample.\"\"\"\n",
    "    return [feature(x) for feature in feature_list]\n",
    "\n",
    "\n",
    "feature_data = sample_data.apply(feature_vector, axis=1).apply(pd.Series)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Relative Sample-Häufigkeit\n",
    "\n",
    "Für das Training des Max-Entropy-Modells müssen für alle Features Erwartungswerte erheben.  \n",
    "Für diese Erwartungwerte wiederum braucht man die Wahrscheinlichkeit des zufälligen Auftretens eines Samples, die hier mithilfe der relativen Häufigkeit unter den Trainingsdaten abgeschätzt werden kann."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# store relative sample frequency\n",
    "\n",
    "sample_data['rel_freq'] = pd.Series(list(\n",
    "    sample_data.apply(tuple, axis=1).value_counts()\n",
    ")) / len(sample_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Feature-Erwartungswerte\n",
    "\n",
    "Die Erwartungswerte ergeben sich dann aus dem folgenden Matrixprodukt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate observed feature expectations\n",
    "# m features\n",
    "# n samples\n",
    "\n",
    "K = np.matmul(                              #        E(f_j) =   , 1 x m\n",
    "    np.array(sample_data['rel_freq']),      #   p(h_i, t_i) =   , 1 x n\n",
    "    feature_data.values                     # f_j(h_i, t_i) =   , n x m\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Instanzierung und Training des Max-Entropy-Modells"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# instantiate max entropy model\n",
    "features = feature_data.values.T\n",
    "samplespace = sample_data.values\n",
    "model = maxentropy.model.Model(features=features, samplespace=samplespace, vectorized=False)\n",
    "\n",
    "# fit model to feature expectations\n",
    "model.fit([K])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# utility functions\n",
    "\n",
    "def generate_tags(word: str):\n",
    "    \"\"\"Generate possible tags from tag dictionary. Defaults to complete Taglist.\"\"\"\n",
    "    return list(tag_dict.get(word, TAGS))\n",
    "    \n",
    "        \n",
    "def get_prob(words: list, tags: list, i: int):\n",
    "    \"\"\"Calculate p(t_i | h_i).\"\"\"\n",
    "    samples = pd.DataFrame(generate_samples(list(zip(words, tags))))\n",
    "    samples.columns = SAMPLE_DATA_COLUMNS\n",
    "    return p(list(samples.iterrows())[i][1].to_dict())\n",
    "\n",
    "\n",
    "def fill_sequence(tags: list, length: int, value=None):\n",
    "    \"\"\"Fill up the given list until length.\"\"\"\n",
    "    return tags + [value] * (length - len(tags))\n",
    "\n",
    "\n",
    "# probability function;\n",
    "# yields the models probabilty p(x) for a given sample x\n",
    "p = model.pmf_function(f=feature_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## KLASSIFIZIERUNG\n",
    "\n",
    "### Beam-Search-Algorithmus\n",
    "\n",
    "Für die Suche nach der Tagsequenz *tags*, die die Wahrscheinlichkeit *p(tags|words)* maximiert, wird der Beam-Search-Algorithmus\n",
    "verwendet. Es handelt sich dabei um eine speicheroptimierte Version des klassischen Greedy-Algorithmus. Als Heuristik wird die Wahrscheinlichkeit des bereits getaggten Teils der Sequenz herangezogen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def search_POS_tags(words: list, N: int = 10):\n",
    "    \"\"\"Perform a beam search to find the most likely tag sequence for a given sequence of words.\n",
    "    \n",
    "    See section \"Search Algorithm\" in the paper for details.\n",
    "    \"\"\"\n",
    "    \n",
    "    len_words = len(words)\n",
    "    \n",
    "    # let s_i,j be the j'th highest probability tag sequence up to and including word w_i.\n",
    "    s = [None] * len_words\n",
    "            \n",
    "    # 1. Generate tags for w_0, find top N, set s_0,j , 0 =< j =< N, accordingly.\n",
    "    gen_tags = generate_tags(word)\n",
    "    s[0] = sorted(\n",
    "        list(zip(tags, [get_prob(words, fill_sequence([tag], len_words), 0) for tag in tags])),\n",
    "        key=lambda x: x[1],\n",
    "        reverse=True\n",
    "    )[:N]\n",
    "    \n",
    "    # 2. Initialize i = 1\n",
    "    for i in range(1, len_words):\n",
    "        for seq, prior in s[i-1]:         \n",
    "            new_seqs = []\n",
    "            for gen_tag in tag_dict.get(words[i], TAGS):\n",
    "        \n",
    "                # 3. Generate tags for w_i, given s_(i-1),j as previous\n",
    "                # tag context, and append each tag to s(i-1),j to make a new sequence\n",
    "                new_seq = seq.split() + [gen_tag]\n",
    "                new_seq_prob = prior * get_prob(words, fill_sequence(new_seq, len_words), i)\n",
    "                new_seqs.append((\" \".join(new_seq), new_seq_prob))\n",
    "        \n",
    "            # Find N highest probability sequences generated\n",
    "            # by above loop, and set s_i,j, 0 <= j <= N-1, accordingly.\n",
    "            s[i] = sorted(new_seqs, key=lambda x: x[1], reverse=True)[:N]\n",
    "        \n",
    "    return s[len_words-1][0][0].split()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## AUSWERTUNG / TESTS\n",
    "\n",
    "Auf den initial seperat gehaltenen Testdaten kann nun die Genauigkeit ausgewertet werden.\n",
    "Genauer wird zum einen die Genauigkeit beim Taggen einzelner Worte, zum anderen die Genauigkeit beim korrekten Taggen ganzer Sätze betrachtet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sentences_test_words = list(map(lambda s: list(map(lambda p: p[0], s)), sentences_test))\n",
    "sentences_test_tags = list(map(lambda s: list(map(lambda p: p[1], s)), sentences_test))\n",
    "\n",
    "results_sentences = []\n",
    "results_words = []\n",
    "for words, tags in zip(sentences_test_words, sentences_test_tags):\n",
    "    result = search_POS_tags(words)\n",
    "    results_sentences.append( result == tags)\n",
    "    for got, expected in zip(result, tags):\n",
    "        results_words.append(got == expected)\n",
    "        \n",
    "\n",
    "print(\"Satz-Accurracy: {}%\".format(round(sum(results_sentences) / len(results_sentences), 2)))\n",
    "print(\"Wort-Accurracy: {}%\".format(round(sum(results_words) / len(results_words), 2)))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
