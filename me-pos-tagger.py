# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext_format_version: '1.2'
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
#   language_info:
#     codemirror_mode:
#       name: ipython
#       version: 3
#     file_extension: .py
#     mimetype: text/x-python
#     name: python
#     nbconvert_exporter: python
#     pygments_lexer: ipython3
#     version: 3.7.0
# ---

# # Maximum Entropy POS Tagger
#
# > __Zusatzaufgabe Linguistische Informatik - Praktische Übung__  
# > *Jonathan Schlue* \#3712892  
# > Dieser Tagger basiert auf dem [Paper von Adwait Ratnaparkhi](http://www.aclweb.org/anthology/W96-0213)

# ---
#
# ## Vorbereitung
#
# Im Projektverzeichnis können in der Konsole mithilfe von [conda](https://conda.io/miniconda.html) alle Abhängigkeiten wie folgt installiert werden:
#
# ```sh
# conda env create --file environment.yml
# source activate li-lab-env
# python
# ```

# +
# imports

import nltk
from nltk.corpus import treebank
import pandas as pd
import numpy as np
import maxentropy
import random
# -

# ## Daten
#
# Als Datengrundlage verwende ich den Penn Treebank Korpus mit dem Universalen Tagset.

# +
# install corpus and tagset

nltk.download('treebank')
nltk.download('universal_tagset')
sentences = list(treebank.tagged_sents(tagset='universal'))
len_sentences = len(sentences)
# -

# ### Split Trainings- und Testdaten

# +
# randomly draw training and test data partitions
random.shuffle(sentences)

# split training and test data
TRAINING_TEST_SPLIT = int(.9 * len_sentences)
sentences_training, sentences_test = sentences[:TRAINING_TEST_SPLIT], sentences[TRAINING_TEST_SPLIT:]
# -

# ### Datenkonvertierung und Termfrequenzen
#

# +
words = [] # tokens
TAGS = set() # unique POS Tags found in corpus
tag_dict = {} # word -> tags dictionary
for sentence in sentences_training:
    for word, tag in sentence:
        
        # collect words and tags
        words.append(word)
        TAGS.add(tag)
        
        # fill the dictionary
        tag_dict_entry = tag_dict.get(tag, set())
        tag_dict_entry.add(tag)
        tag_dict[word] = tag_dict_entry
        
# calculate token frequencies
freq = pd.Series(words).value_counts()
# -

# ---
#
# ## TRAINING
#
# ### Berechnung des Sample-Spaces
#
# Zunächst werden aus den Annotierten Sätzen der Trainingsdaten Samples erhoben.  
# Jedes Sample besteht dabei aus einem POS-Tag *t_i* und einem Kontext *h_i*, der sich konkret in  
# umliegenden Wörtern und POS-Tags veräußert.

# +
def generate_samples(sentence: list) -> list:
    """Generate data samples from sentence.
    
    Each sample contains the context of words (window size of 5)
    and the two preceeding tags.
    """
    
    
    def get(key: int, index: int, sentence: list) -> str:
        """Extract word or tag from the token at position `index` in the given sentence.
        
        Use key 0 to get the word, key 1 to get the token.
        Defaults to None, if the index is out of bounds.
        """
        return sentence[index][key] if 0 <= index and index < len(sentence) else None
    
    
    samples = []
    for i in range(len(sentence)):     
        context = []
        target = get(1, i, sentence)
        word_window = range(i-2, i+3)
        tag_window = range(i-2, i)
        context += [get(0, j, sentence) for j in word_window]
        context += [get(1, j, sentence) for j in tag_window]
        samples.append(context + [target])
    return samples

# generate samples from corpus

sample_data = []    
for sentence in sentences_training:
    [sample_data.append(sample) for sample in generate_samples(sentence)]
sample_data = pd.DataFrame(sample_data)
SAMPLE_DATA_COLUMNS = ["w_i-2", "w_i-1", "w_i", "w_i+1", "w_i+2", "t_i-2", "t_i-1", "t_i"]
sample_data.columns = SAMPLE_DATA_COLUMNS

sample_data[:10]
# -

# ### Unterscheidung von seltenen und nicht seltenenen Wörtern
#
# Ich folge hier der Annahme im Paper, dass selten Wortformen, die in den Trainingsdaten selten vorkommen, sich ähnlich verhalten wir Wortformen, die in den Testdaten erstmals auftreten.
#
# Diese Unterscheidung schlägt sich maßgeblich in der folgenden Auswahl der Feature-Templates wieder.

# +
# split into rare and non-rare tokens

RARE_TOKEN_SPLIT = 5
rare = list(freq[freq < RARE_TOKEN_SPLIT].index)

sample_data['w_i_rare'] = sample_data['w_i'].isin(rare)
# -

# ### Feature-Templates
#
# Die Feature-Templates sind direkte Implementierungen der Templates in Tabelle 1 im Paper. Die Modellierung mit Identifizierungstupeln dient im nächsten Schritt der Auschlöschung doppelter, identischer Features.

# +
# define feature templates

# not rare
ft0  = lambda X, T:    (( 0, X, T),    lambda x: x['w_i'] == X                       and x['t_i'] == T)

# rare
ft1  = lambda X, T:    (( 1, X, T),    lambda x: x['w_i'].startswith(X)              and x['t_i'] == T)
ft2  = lambda X, T:    (( 2, X, T),    lambda x: x['w_i'].endswith(X)                and x['t_i'] == T)
ft3  = lambda T:       (( 3, T),       lambda x: any(c.isdigit() for c in x['w_i'])  and x['t_i'] == T)
ft4  = lambda T:       (( 4, T),       lambda x: any(c.isupper() for c in x['w_i'])  and x['t_i'] == T)
ft5  = lambda T:       (( 5, T),       lambda x: '-' in x['w_i']                     and x['t_i'] == T)

# all
ft6  = lambda X, T:    (( 6, T),       lambda x: x['t_i-1'] == X                     and x['t_i'] == T)
ft7  = lambda X, Y, T: (( 7, X, Y, T), lambda x: x['t_i-2'] == X and x['t_i-1'] == Y and x['t_i'] == T)
ft8  = lambda X, T:    (( 8, X, T),    lambda x: x['w_i-1'] == X                     and x['t_i'] == T)
ft9  = lambda X, T:    (( 9, X, T),    lambda x: x['w_i-2'] == X                     and x['t_i'] == T)
ft10 = lambda X, T:    ((10, X, T),    lambda x: x['w_i+1'] == X                     and x['t_i'] == T)
ft11 = lambda X, T:    ((11, X, T),    lambda x: x['w_i+2'] == X                     and x['t_i'] == T)
# -

# ### Feature-Instanzierung
#
# Ausgehend von den oben implementierten Feature-Templates können nun anhand der Trainingsdaten konkrete Features abgeleitet werden. Je nach Häufigkeit des zentralen Wortes werden hier pro Kontext unterschiedliche Features instanziert.
#
# Das Dictionary dient hier der o.g. Dupkikatvermeidung von Features.

# +
feature_dict = {}


def prefixes(w: str) -> list:
    """Returns 0 to 4 non-empty prefixes of w"""
    return [w[:l] for l in range(1, 1 + min(len(w), 4))]


def suffixes(w: str) -> list:
    """Returns 0 to 4 non-empty suffixes of w"""
    return [w[-l:] for l in range(1, 1 + min(len(w), 4))]
    
    
def instantiate_features(s: pd.Series) -> None:
    """Instantiate features necessary to encode this sample."""
    features = []
    if not s['w_i_rare']:
        # non rare
        # TODO do not filter ft0 instances as rare
        features.append(ft0(s['w_i'], s['t_i']))
    else:
        
        # rare
        features += [ft1(prefix, s['t_i']) for prefix in prefixes(s['w_i'])]
        features += [ft2(suffix, s['t_i']) for suffix in suffixes(s['w_i'])]
        features.append(ft3(s['t_i']))
        features.append(ft4(s['t_i']))
        features.append(ft5(s['t_i']))
        
    # all
    features.append( ft6(s['t_i-1'], s['t_i']))
    features.append( ft7(s['t_i-2'], s['t_i-1'], s['t_i']))
    features.append( ft8(s['w_i-1'], s['t_i']))
    features.append( ft9(s['w_i-2'], s['t_i']))
    features.append(ft10(s['w_i+1'], s['t_i']))
    features.append(ft11(s['w_i+2'], s['t_i']))
    
    # globally collect features and remove duplicates
    for feature in features:
        feature_dict[feature[0]] = feature[1]
        

sample_data.apply(instantiate_features, axis=1)
feature_list = list(feature_dict.values())
feature_list[:10]
# -

# ### Berechnung des Feature-Spaces

# +
def feature_vector(x: pd.Series) -> list:
    """Calculate the feature vector for a given sample."""
    return [feature(x) for feature in feature_list]


feature_data = sample_data.apply(feature_vector, axis=1).apply(pd.Series)
# -

# ### Relative Sample-Häufigkeit
#
# Für das Training des Max-Entropy-Modells müssen für alle Features Erwartungswerte erheben.  
# Für diese Erwartungwerte wiederum braucht man die Wahrscheinlichkeit des zufälligen Auftretens eines Samples, die hier mithilfe der relativen Häufigkeit unter den Trainingsdaten abgeschätzt werden kann.

# +
# store relative sample frequency

sample_data['rel_freq'] = pd.Series(list(
    sample_data.apply(tuple, axis=1).value_counts()
)) / len(sample_data)
# -

# ### Feature-Erwartungswerte
#
# Die Erwartungswerte ergeben sich dann aus dem folgenden Matrixprodukt:

# +
# calculate observed feature expectations
# m features
# n samples

K = np.matmul(                              #        E(f_j) =   , 1 x m
    np.array(sample_data['rel_freq']),      #   p(h_i, t_i) =   , 1 x n
    feature_data.values                     # f_j(h_i, t_i) =   , n x m
)
# -

# ## Instanzierung und Training des Max-Entropy-Modells

# +
# instantiate max entropy model
features = feature_data.values.T
samplespace = sample_data.values
model = maxentropy.model.Model(features=features, samplespace=samplespace, vectorized=False)

# fit model to feature expectations
model.fit([K])

# +
# utility functions

def generate_tags(word: str):
    """Generate possible tags from tag dictionary. Defaults to complete Taglist."""
    return list(tag_dict.get(word, TAGS))
    
        
def get_prob(words: list, tags: list, i: int):
    """Calculate p(t_i | h_i)."""
    samples = pd.DataFrame(generate_samples(list(zip(words, tags))))
    samples.columns = SAMPLE_DATA_COLUMNS
    return p(list(samples.iterrows())[i][1].to_dict())


def fill_sequence(tags: list, length: int, value=None):
    """Fill up the given list until length."""
    return tags + [value] * (length - len(tags))


# probability function;
# yields the models probabilty p(x) for a given sample x
p = model.pmf_function(f=feature_list)
# -

# ## KLASSIFIZIERUNG
#
# ### Beam-Search-Algorithmus
#
# Für die Suche nach der Tagsequenz *tags*, die die Wahrscheinlichkeit *p(tags|words)* maximiert, wird der Beam-Search-Algorithmus
# verwendet. Es handelt sich dabei um eine speicheroptimierte Version des klassischen Greedy-Algorithmus. Als Heuristik wird die Wahrscheinlichkeit des bereits getaggten Teils der Sequenz herangezogen.

def search_POS_tags(words: list, N: int = 10):
    """Perform a beam search to find the most likely tag sequence for a given sequence of words.
    
    See section "Search Algorithm" in the paper for details.
    """
    
    len_words = len(words)
    
    # let s_i,j be the j'th highest probability tag sequence up to and including word w_i.
    s = [None] * len_words
            
    # 1. Generate tags for w_0, find top N, set s_0,j , 0 =< j =< N, accordingly.
    gen_tags = generate_tags(word)
    s[0] = sorted(
        list(zip(tags, [get_prob(words, fill_sequence([tag], len_words), 0) for tag in tags])),
        key=lambda x: x[1],
        reverse=True
    )[:N]
    
    # 2. Initialize i = 1
    for i in range(1, len_words):
        for seq, prior in s[i-1]:         
            new_seqs = []
            for gen_tag in tag_dict.get(words[i], TAGS):
        
                # 3. Generate tags for w_i, given s_(i-1),j as previous
                # tag context, and append each tag to s(i-1),j to make a new sequence
                new_seq = seq.split() + [gen_tag]
                new_seq_prob = prior * get_prob(words, fill_sequence(new_seq, len_words), i)
                new_seqs.append((" ".join(new_seq), new_seq_prob))
        
            # Find N highest probability sequences generated
            # by above loop, and set s_i,j, 0 <= j <= N-1, accordingly.
            s[i] = sorted(new_seqs, key=lambda x: x[1], reverse=True)[:N]
        
    return s[len_words-1][0][0].split()

# ## AUSWERTUNG / TESTS
#
# Auf den initial seperat gehaltenen Testdaten kann nun die Genauigkeit ausgewertet werden.
# Genauer wird zum einen die Genauigkeit beim Taggen einzelner Worte, zum anderen die Genauigkeit beim korrekten Taggen ganzer Sätze betrachtet.

# +
sentences_test_words = list(map(lambda s: list(map(lambda p: p[0], s)), sentences_test))
sentences_test_tags = list(map(lambda s: list(map(lambda p: p[1], s)), sentences_test))

results_sentences = []
results_words = []
for words, tags in zip(sentences_test_words, sentences_test_tags):
    result = search_POS_tags(words)
    results_sentences.append( result == tags)
    for got, expected in zip(result, tags):
        results_words.append(got == expected)
        

print("Satz-Accurracy: {}%".format(round(sum(results_sentences) / len(results_sentences), 2)))
print("Wort-Accurracy: {}%".format(round(sum(results_words) / len(results_words), 2)))
