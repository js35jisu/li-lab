# li-lab

> Zusatzaufgabe Linguistische Informatik - Praktische Übung

## Installation

```sh
conda env create --file environment.yml
source activate li-lab-env
pip install https://github.com/jonathanschlue/maxentropy/archive/master.zip
```

## Ausführung

### Jupyter

```sh
source activate li-lab-env
jupyter POS-ME.ipynb
```

---

> Gehosted im [Informatik-Gitlab](https://git.informatik.uni-leipzig.de/js35jisu/li-lab)